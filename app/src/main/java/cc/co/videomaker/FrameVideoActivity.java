package cc.co.videomaker;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer.ExoPlayer;
import com.hiep.video.maker.R;

import cc.co.videomaker.entity.AudioEntity;
import cc.co.videomaker.entity.VideoEntity;
import cc.co.videomaker.music.MusicActivity;
import cc.co.videomaker.player.DemoPlayer;
import cc.co.videomaker.player.ExtractorRendererBuilder;
import cc.co.videomaker.system.AppConfig;
import cc.co.videomaker.ui.adapter.MyAdapter;
import cc.co.videomaker.util.BitmapUtil;
import cc.co.videomaker.util.FFmpegCmdUtil;
import cc.co.videomaker.util.FileUtil;
import cc.co.videomaker.util.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by anh on 7/24/2016.
 */
public class FrameVideoActivity extends BaseActivity implements SurfaceHolder.Callback, DemoPlayer.Listener {
    private static final String TAG = FrameVideoActivity.class.getSimpleName();
    private static final int KEY_CMD_ADD_FRAME_VIDEO = 2;
    private static final int KEY_CMD_ADD_AUDIO_VIDEO = 5;
    private static final int KEY_CMD_CREATE_VIDEO = 9;
    public static final int REQUEST_CODE_AUDIO = 3;
    public static final int HANDLE_SHOW_VIDEO_DURATION = 1;


    private boolean isAddAudioVideo = false;


    private LinearLayout mBtnSelectAudio;
    private LinearLayout mBtnSelectTime;
    private LinearLayout mBtnSelectFrame;
    private RelativeLayout mRLFrame;

    private LinearLayout mLlBack;
    private LinearLayout mLlDone;
    private SurfaceView mSvVideo;
    private ImageView mIvStatus;
    private ImageView mIvFrame;
    private RecyclerView recyclerFrameEffect;
    private ProgressBar mPgDuration;
    private ProgressDialog progressDialog;

    private FFmpeg ffmpeg;
    private DemoPlayer player;
    private Uri contentUri;

    private String videoInput;
    private String videoOutput;
    private int widthVideo, heightVideo;
    private boolean isAddFrame = false;
    private boolean done = false;
    private Spinner mSpinner;
    private TextView tvTime;
    private ArrayList<String> list = new ArrayList<>();
    private ArrayList<Float> listApply = new ArrayList<>();
    private ArrayAdapter<String> mAdapter;
    public String audio = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_video);
        ffmpeg = FFmpeg.getInstance(getApplicationContext());
        iniUI();

        loadFFMpegBinary();
        // createVideoByFrame(1.0F);

        //will set time 1 s by default
        setTimeAdapter();
        showAdmobFullScreend();
        addAdmobBaner(R.id.ll_native_ads);

    }

    private void setTimeAdapter() {
        list.add("1 Sec");
        list.add("1.5 Sec");
        list.add("2.0 Sec");
        list.add("2.5 Sec");
        list.add("3.0 Sec");
        list.add("3.5 Sec");
        list.add("4.0 Sec");
        list.add("4.5 Sec");
        list.add("5.0 Sec");
        list.add("5.5 Sec");
        list.add("6.0 Sec");
        list.add("6.5 Sec");
        list.add("7.0 Sec");
        list.add("7.5 Sec");
        list.add("8.0 Sec");
        list.add("8.5 Sec");
        list.add("9.0 Sec");
        list.add("9.5 Sec");
        list.add("10 Sec");


        listApply.add(1F);
        listApply.add(1.5F);
        listApply.add(2.0F);
        listApply.add(2.5F);
        listApply.add(3.0F);
        listApply.add(3.5F);
        listApply.add(4.0F);
        listApply.add(4.5F);
        listApply.add(5.0F);
        listApply.add(5.5F);
        listApply.add(6.0F);
        listApply.add(6.5F);
        listApply.add(7.0F);
        listApply.add(7.5F);
        listApply.add(8.0F);
        listApply.add(8.5F);
        listApply.add(9.0F);
        listApply.add(9.5F);
        listApply.add(10F);

        mAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        mSpinner.setAdapter(mAdapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                createVideoByFrame(listApply.get(i));
                tvTime.setText(list.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d("onActivityResult");
        if (data != null && resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_AUDIO) {
                AudioEntity audioEntity = (AudioEntity) data.getSerializableExtra("audio_select");
                if (audioEntity != null) {
                    if (player != null && player.getPlayerControl() != null && player.getPlayerControl().isPlaying()) {
                        player.getPlayerControl().pause();
                    }
                    audio = audioEntity.getAudioPath();
                    createVideoByFrame(listApply.get(mSpinner.getSelectedItemPosition()));
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setAudio() {
        if (!audio.equals("")) {
            if (player != null && player.getPlayerControl() != null && player.getPlayerControl().isPlaying()) {
                player.getPlayerControl().pause();
            }
            addAudioToVideo(videoInput, audio);
        }
    }

    public void iniUI() {
        super.iniUI();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);

        mLlBack = (LinearLayout) findViewById(R.id.ll_video_frame_back);
        mLlDone = (LinearLayout) findViewById(R.id.ll_video_frame_done);
        mSvVideo = (SurfaceView) findViewById(R.id.sv_play_frame_video);
        mIvStatus = (ImageView) findViewById(R.id.iv_play_frame_status);
        mIvFrame = (ImageView) findViewById(R.id.iv_frame_video);
        mPgDuration = (ProgressBar) findViewById(R.id.pb_preview_video_duration);
        recyclerFrameEffect = (RecyclerView) findViewById(R.id.recycler_frame_video);

        mSpinner = (Spinner) findViewById(R.id.spinenr);
        tvTime = (TextView) findViewById(R.id.tv_time);

        mBtnSelectAudio = (LinearLayout) findViewById(R.id.btn_select_audio);
        mBtnSelectFrame = (LinearLayout) findViewById(R.id.btn_select_frame);
        mBtnSelectTime = (LinearLayout) findViewById(R.id.btn_select_time);
        mRLFrame = (RelativeLayout) findViewById(R.id.rl_frames);

        mLlBack.setOnClickListener(this);
        mLlDone.setOnClickListener(this);
        mBtnSelectFrame.setOnClickListener(this);
        mBtnSelectAudio.setOnClickListener(this);
        mBtnSelectTime.setOnClickListener(this);
        mRLFrame.setOnClickListener(this);

        mSvVideo.getHolder().addCallback(this);

        mIvStatus.setVisibility(View.INVISIBLE);
    }

    private void loadFFMpegBinary() {
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(FrameVideoActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.device_not_supported))
                .setMessage(getString(R.string.device_not_supported_message))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FrameVideoActivity.this.finish();
                    }
                })
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videoInput != null && !TextUtils.isEmpty(videoInput)) {
            contentUri = Uri.parse(videoInput);
            preparePlayer();
        }
    }

    /// Add UI boder
    int[] iconBorderlist;

    private void iniFrame() {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Bitmap bmp = null;
        try {
            retriever.setDataSource(videoInput);
            bmp = retriever.getFrameAtTime();
            widthVideo = bmp.getHeight();
            heightVideo = bmp.getWidth();
            Logger.d("widthVideo: " + widthVideo + " > heightVideo: " + heightVideo);
        } catch (Exception e) {

        }
        iconBorderlist = new int[]{
                R.mipmap.ic_none, R.mipmap.boder1, R.mipmap.boder2, R.mipmap.boder27, R.mipmap.boder28,
                R.mipmap.boder3, R.mipmap.boder9, R.mipmap.boder15, R.mipmap.boder21, R.mipmap.boder29,
                R.mipmap.boder4, R.mipmap.boder10, R.mipmap.boder16, R.mipmap.boder22, R.mipmap.boder30,
                R.mipmap.boder5, R.mipmap.boder11, R.mipmap.boder17, R.mipmap.boder23,
                R.mipmap.boder6, R.mipmap.boder12, R.mipmap.boder18, R.mipmap.boder24,
                R.mipmap.boder7, R.mipmap.boder13, R.mipmap.boder19, R.mipmap.boder25,
                R.mipmap.boder8, R.mipmap.boder14, R.mipmap.boder20, R.mipmap.boder26
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(linearLayoutManager.HORIZONTAL);
        int colorFlipper = this.getResources().getColor(R.color.trgb_262626);
        int colorBtnFooter = this.getResources().getColor(R.color.collage_purple);
        MyAdapter collageAdapter = new MyAdapter(iconBorderlist, new MyAdapter.CurrentCollageIndexChangedListener() {
            public void onIndexChanged(int index) {
                if (index > 0) {
                    Glide.with(FrameVideoActivity.this).load(iconBorderlist[index]).asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                    mIvFrame.setImageResource(android.R.color.transparent);
                                    isAddFrame = false;
                                }

                                @Override
                                public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                    mIvFrame.setImageBitmap(arg0);
                                    FileUtil.deleteFileInDir(FileUtil.getImageBorder());
                                    Bitmap bm = BitmapUtil.scaleBitmap(arg0, widthVideo, heightVideo);
                                    BitmapUtil.saveBitmapNoCompression(bm, FFmpegCmdUtil.linkBorder);
                                    isAddFrame = true;
                                }
                            });

                } else if (index == 0) {
                    mIvFrame.setImageResource(android.R.color.transparent);
                    isAddFrame = false;
                }
            }
        }, colorFlipper, colorBtnFooter, false, true);

        recyclerFrameEffect.setLayoutManager(linearLayoutManager);
        recyclerFrameEffect.setAdapter(collageAdapter);
        recyclerFrameEffect.setItemAnimator(new DefaultItemAnimator());
    }

    private void createVideoByFrame(float sec) {
        int size = new File(FileUtil.getImageInput()).listFiles().length;
        Logger.d("Size input frame: " + size);
        String pathInputImage = FileUtil.getImageInput() + "/%d.jpg";
        videoInput = FileUtil.getSlideVideo() + "/slide_video_" + System.currentTimeMillis() + ".mp4";
        execFFmpegBinary(FFmpegCmdUtil.cmdCreateVideo(sec, size, pathInputImage, videoInput), KEY_CMD_CREATE_VIDEO);
    }

    public void addAudioToVideo(String linkvideo, String linkAudio) {
        videoInput = FileUtil.getSlideVideo() + "/slide_video_" + System.currentTimeMillis() + ".mp4";
        execFFmpegBinary(FFmpegCmdUtil.cmdAddAudiotoVideo(linkvideo, linkAudio, videoInput), KEY_CMD_ADD_AUDIO_VIDEO);
    }

    public void addFrameToVideo(String path) {

        //videoOutput = FileUtil.getSlideVideo() + "/video_maker_" + System.currentTimeMillis() + ".mp4";
        execFFmpegBinary(FFmpegCmdUtil.cmdAddBorderToVideo(videoInput, path), KEY_CMD_ADD_FRAME_VIDEO);
    }

    private void execFFmpegBinary(final String[] command, final int key) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    if (key == KEY_CMD_ADD_FRAME_VIDEO) {
                        Toast.makeText(FrameVideoActivity.this, "Add  frame failure", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onSuccess(String s) {
                    if (key == KEY_CMD_CREATE_VIDEO) {
                        if (new File(videoInput).exists()) {
                            contentUri = Uri.parse(videoInput);
                            preparePlayer();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setAudio();
                                    iniFrame();
                                }
                            });
                        }
                    } else if (key == KEY_CMD_ADD_FRAME_VIDEO) {
                        if (done) {
                            gotoPreview(videoOutput);
                        }
                    } else if (key == KEY_CMD_ADD_AUDIO_VIDEO) {
                        if (new File(videoInput).exists()) {
                            contentUri = Uri.parse(videoInput);
                            preparePlayer();
                            isAddAudioVideo = true;
                        }
                    }
                }

                @Override
                public void onProgress(String s) {
                    Logger.d(TAG, "Started command : ffmpeg " + command);
                    progressDialog.setMessage("Processing\n" + s);
                }

                @Override
                public void onStart() {
                    Logger.d(TAG, "Started command : ffmpeg " + command);
                    progressDialog.setMessage("Processing...");
                    progressDialog.show();
                }

                @Override
                public void onFinish() {
                    Logger.d(TAG, "Finished command : ffmpeg " + command);
                    progressDialog.dismiss();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ll_video_frame_back:
                onBackPressed();
                break;
            case R.id.btn_select_audio:
                if (mRLFrame.getVisibility() == View.VISIBLE)
                    mRLFrame.setVisibility(View.GONE);

                Intent intent = new Intent(FrameVideoActivity.this, MusicActivity.class);
                startActivityForResult(intent, REQUEST_CODE_AUDIO);
                break;
            case R.id.btn_select_time:
                if (mRLFrame.getVisibility() == View.VISIBLE)
                    mRLFrame.setVisibility(View.GONE);

                mSpinner.performClick();
                break;
            case R.id.btn_select_frame:
                if (mRLFrame.getVisibility() == View.VISIBLE)
                    mRLFrame.setVisibility(View.GONE);
                else mRLFrame.setVisibility(View.VISIBLE);
                break;
            case R.id.ll_video_frame_done:
                if (isAddFrame) {
                    createVideo();
                    done = true;
                } else {
                    try {
                       createVideo2();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;
        }
    }

    public void createVideo() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("Video Name");
        dialogBuilder.setMessage("Enter video name");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                videoOutput = FileUtil.getMyVideo() + "/video_maker_" + System.currentTimeMillis() + ".mp4";
                if (!edt.getText().toString().equals(""))
                    videoOutput = FileUtil.getMyVideo()+ "/" + edt.getText().toString() + ".mp4";

                addFrameToVideo(videoOutput);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void createVideo2() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("Video Name");
        dialogBuilder.setMessage("Enter video name");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                videoOutput = FileUtil.getMyVideo() + "/video_maker_" + System.currentTimeMillis() + ".mp4";
                if (!edt.getText().toString().equals(""))
                    videoOutput = FileUtil.getMyVideo() + "/" + edt.getText().toString() + ".mp4";

                try {
                    CopyFile(new File(videoInput), new File(videoOutput));
                    if (isAddAudioVideo) {
                        gotoPreview(videoInput);
                    } else
                        gotoPreview(videoInput);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    private void gotoPreview(String path) {
        File childFile = new File(path);
        if (childFile.exists()) {
            VideoEntity videoEntity = new VideoEntity();
            videoEntity.setId(0);
            videoEntity.setCreateTime(childFile.lastModified());
            Logger.d(TAG, "Create time: " + videoEntity.getCreateTime());
            videoEntity.setFilePath(childFile.getAbsolutePath());
            videoEntity.setFileName(childFile.getName());

            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            retriever.setDataSource(FrameVideoActivity.this, Uri.fromFile(childFile));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            Logger.d(TAG, "Duration time: " + time);
            videoEntity.setDuration(Long.parseLong(time));

            Intent intent = new Intent(FrameVideoActivity.this, PreviewVideoActivity.class);
            intent.putExtra(AppConfig.EXTRA_VIDEO_ENTITY, videoEntity);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            return;
        }
    }

    private DemoPlayer.RendererBuilder getRendererBuilder() {
        String userAgent = com.google.android.exoplayer.util.Util.getUserAgent(this, "ExoPlayerDemo");
        return new ExtractorRendererBuilder(this, userAgent, contentUri);
    }

    private void preparePlayer() {
        if (player != null)
            player.release();

        player = new DemoPlayer(getRendererBuilder());
        player.addListener(this);
        player.seekTo(0);
        if (player != null)
            player.prepare();

        player.setSurface(mSvVideo.getHolder().getSurface());
        player.setPlayWhenReady(true);
        mIvStatus.setVisibility(View.INVISIBLE);
    }

    private void releasePlayer() {
        handler.removeMessages(HANDLE_SHOW_VIDEO_DURATION);
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HANDLE_SHOW_VIDEO_DURATION:
                    if (player != null) {
                        mPgDuration.setProgress((int) ((player.getCurrentPosition() * 1000) / player.getDuration()));
                        handler.sendEmptyMessageDelayed(HANDLE_SHOW_VIDEO_DURATION, 100);
                    }
                    break;
            }
        }
    };

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (player != null) {
            player.setSurface(surfaceHolder.getSurface());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void onStateChanged(final boolean playWhenReady, final int playbackState) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String text = "playWhenReady=" + playWhenReady + ", playbackState=";
                switch (playbackState) {
                    case ExoPlayer.STATE_BUFFERING:
                        text += "buffering";
                        break;
                    case ExoPlayer.STATE_ENDED:
                        releasePlayer();
                        if (!isFinishing() && (videoInput == null || !new File(videoInput).exists())) {
                            contentUri = Uri.parse(videoInput);
                            preparePlayer();
                        } else if (!isFinishing() && videoInput != null && new File(videoInput).exists()) {
                            contentUri = Uri.parse(videoInput);
                            preparePlayer();
                        }
                        text += "ended";
                        break;
                    case ExoPlayer.STATE_IDLE:
                        text += "idle";
                        break;
                    case ExoPlayer.STATE_PREPARING:
                        text += "preparing";
                        break;
                    case ExoPlayer.STATE_READY:
                        text += "ready";
                        handler.sendEmptyMessage(HANDLE_SHOW_VIDEO_DURATION);
                        break;
                    default:
                        text += "unknown";
                        break;
                }
                Logger.d(TAG, "exoplayer " + text);
            }
        });
    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onBackPressed() {
        releasePlayer();
        finish();
    }

    @Override
    protected void onPause() {
        releasePlayer();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    public static void CopyFile(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {

                CopyFile(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);

            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }

    }
}
