package cc.co.videomaker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hiep.video.maker.R;
import com.neurenor.permissions.PermissionCallback;
import com.neurenor.permissions.PermissionsHelper;

import java.util.HashMap;

import cc.co.videomaker.merge.SelectVideoMergeActivity;
import cc.co.videomaker.system.Config;
import cc.co.videomaker.util.FileUtil;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by hiep on 6/20/2016.
 */
public class HomeActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private LinearLayout mBtnSelectImage;
    private LinearLayout mBtnMyVideo;
    private LinearLayout mBtnMergeVideo;
    private Toolbar mToolbar;

    private ProgressDialog progressDialog;
    PermissionsHelper helper;

    private void checkPermission() {

        helper = new PermissionsHelper(this);

        helper.requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, new PermissionCallback() {
            @Override
            public void onResponseReceived(final HashMap<String, PermissionsHelper.PermissionGrant> mapPermissionGrants) {
                PermissionsHelper.PermissionGrant permissionGrant = mapPermissionGrants
                        .get(WRITE_EXTERNAL_STORAGE);

                switch (permissionGrant) {
                    case GRANTED:

                        //permission has been granted
                        //Toast.makeText(SelectImageActivity.this,"Granted",Toast.LENGTH_SHORT).show();
                        break;
                    case DENIED:
                        //permission has been denied
                        Toast.makeText(HomeActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case NEVERSHOW:
                        //permission has been denied and never show has been selected. Open permission settings of the app.
                        //Toast.makeText(SelectImageActivity.this,"Denied with Never show", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        helper.onRequestPermissionsResult(permissions, grantResults);
    }

    public static final String EXTAR_PHOTO_PATH_LIST = "photo_id_list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Config.init(HomeActivity.this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                FileUtil.createVideoFolders();
                FileUtil.copyVideoEffect();
            }
        }).start();
        setContentView(R.layout.activity_home);
        iniUI2();
        checkPermission();
        // getSongListFromStorage();

        initAdd();
    }

    public void initAdd(){
        addAdmobBaner(R.id.ll_native_ads);
    }

    public void iniUI2() {
        mBtnSelectImage = (LinearLayout) findViewById(R.id.btn_select_image);
        mBtnMyVideo = (LinearLayout) findViewById(R.id.btn_my_video);
        mBtnMergeVideo = (LinearLayout) findViewById(R.id.btn_merge_video);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mBtnSelectImage.setOnClickListener(this);
        mBtnMyVideo.setOnClickListener(this);
        mBtnMergeVideo.setOnClickListener(this);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);

        mToolbar.setTitle("SlideShow");
        mToolbar.setTitleTextColor(Color.RED);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select_image:
                startActivity(new Intent(HomeActivity.this, SelectImageActivity.class));
                break;
            case R.id.btn_my_video:
                startActivity(new Intent(HomeActivity.this, MyVideoActivity.class));
                break;
            case R.id.btn_merge_video:
                Toast.makeText(getApplicationContext(),"Coming soon",Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(HomeActivity.this, SelectVideoMergeActivity.class));
                break;

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
