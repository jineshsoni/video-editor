package cc.co.videomaker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.hiep.video.maker.R;

import cc.co.videomaker.gallery.GalleryFragment;
import cc.co.videomaker.system.Config;
import cc.co.videomaker.ui.EditorActivity;
import cc.co.videomaker.util.FileUtil;

import java.util.ArrayList;

/**
 * Created by hiep on 6/20/2016.
 */
public class SelectImageActivity extends BaseActivity{
    private static final String TAG=SelectImageActivity.class.getSimpleName();

    private GalleryFragment galleryFragment;
    private ProgressDialog progressDialog;

    public static final String EXTAR_PHOTO_PATH_LIST = "photo_id_list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Config.init(SelectImageActivity.this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                FileUtil.createVideoFolders();
                FileUtil.copyVideoEffect();
            }
        }).start();
        setContentView(R.layout.activity_select_image);
        iniUI();
        addGalleryFragment();
        showAdmobFullScreend();
    }
    public void iniUI(){

        this.galleryFragment = (GalleryFragment) getSupportFragmentManager().findFragmentByTag("myFragmentTag");
        if (this.galleryFragment != null) {
            this.galleryFragment.setGalleryListener(createGalleryListener());
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(null);
    }

    private long[] idImageGalleries;

    GalleryFragment.GalleryListener createGalleryListener() {
        return new GalleryFragment.GalleryListener() {
            public void onGalleryCancel() {
                SelectImageActivity.this.getSupportFragmentManager().beginTransaction().hide(SelectImageActivity.this.galleryFragment).commitAllowingStateLoss();
            }

            public void onGalleryOkImageArray(long[] paramArrayOfLong, int[] paramArrayOfInt, boolean paramBoolean) {
                idImageGalleries=paramArrayOfLong;
                copyFileImage();
            }

            public void onGalleryOkImageArrayRemoveFragment(long[] paramArrayOfLong, int[] paramArrayOfInt, boolean paramBoolean) {
            }

            public void onGalleryOkSingleImage(long paramLong, int paramInt, boolean paramBoolean) {
            }
        };
    }

    private void copyFileImage(){

        FileUtil.deleteFileInDir(FileUtil.getImageInput());

        ArrayList<String> pathImageGallery=new ArrayList<>();
        if (idImageGalleries!=null && idImageGalleries.length>0){
            for (long id : idImageGalleries){
                Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Long.toString(id));
                String path= FileUtil.getRealPathFromURI(SelectImageActivity.this,uri);
                if (path!=null){

                    Log.d(TAG,"----> path: "+path);
                    pathImageGallery.add(path);
                }
            }
        }

        FileUtil.deleteFileInDir(FileUtil.getSlideVideo());

        //SelectImageActivity.this.getSupportFragmentManager().beginTransaction().hide(SelectImageActivity.this.galleryFragment).commitAllowingStateLoss();
        Intent localIntent = new Intent(SelectImageActivity.this, EditorActivity.class);
        localIntent.putExtra(EXTAR_PHOTO_PATH_LIST,pathImageGallery);
        SelectImageActivity.this.startActivity(localIntent);
        //finish();
    }

    public void addGalleryFragment() {
        FragmentManager fm = getSupportFragmentManager();
        this.galleryFragment = (GalleryFragment) fm.findFragmentByTag("myFragmentTag");
        if (this.galleryFragment == null) {
            this.galleryFragment = new GalleryFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fl_gallery, this.galleryFragment, "myFragmentTag");
            ft.commitAllowingStateLoss();
            this.galleryFragment.setGalleryListener(createGalleryListener());
            findViewById(R.id.fl_gallery).bringToFront();
            return;
        }
        getSupportFragmentManager().beginTransaction().show(this.galleryFragment).commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
