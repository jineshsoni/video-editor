package cc.co.videomaker.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hiep.video.maker.R;

import java.util.ArrayList;

import cc.co.videomaker.system.Config;
import cc.co.videomaker.ui.EditorActivity;
import cc.co.videomaker.util.BitmapUtil;

/**
 * Created by Jinesh Soni on 16/07/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private ArrayList<String> dataSet;
    EditorActivity.OnRemoveImg onRemoveImg;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivGrid;
        ImageView ivCancel;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.ivCancel = (ImageView) itemView.findViewById(R.id.ivCancel);
            this.ivGrid = (ImageView) itemView.findViewById(R.id.ivImg);

        }
    }

    public CustomAdapter(ArrayList<String> data,EditorActivity.OnRemoveImg onRemoveImg) {
        this.dataSet = data;
        this.onRemoveImg=onRemoveImg;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_single, parent, false);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        ImageView ivGrid = holder.ivGrid;
        ImageView ivCancel = holder.ivCancel;

        ivGrid.setImageBitmap(getBackground(dataSet.get(listPosition)));

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRemoveImg.onRemove(listPosition);
            }
        });
        ivGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRemoveImg.onClick(listPosition);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    private Bitmap getBackground(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bitmap = BitmapUtil.resampleImage(filePath, Config.SCREENWIDTH);

        return bitmap;

    }
}
