package cc.co.videomaker.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.internal.l;
import com.hiep.video.maker.R;

import java.util.ArrayList;

import cc.co.videomaker.system.Config;
import cc.co.videomaker.ui.EditorActivity;
import cc.co.videomaker.util.BitmapUtil;
import cc.co.videomaker.util.LayoutUtil;

/**
 * Created by Jinesh Soni on 15/07/2017.
 */

public class PhotoGridAdapter extends BaseAdapter {

    ArrayList<String> listPathFrames = new ArrayList<>();
    Activity activity;
    EditorActivity.OnRemoveImg onRemoveImg;

    public PhotoGridAdapter(ArrayList<String> listPathFrames, Activity activity, EditorActivity.OnRemoveImg onRemoveImg) {
        this.listPathFrames = listPathFrames;
        this.activity = activity;
        this.onRemoveImg = onRemoveImg;
    }

    @Override
    public int getCount() {
        return listPathFrames.size();
    }

    @Override
    public Object getItem(int i) {
        return listPathFrames.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int pos, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.grid_single, parent, false);

        ImageView ivImg = (ImageView) rowView.findViewById(R.id.ivImg);
        ImageView ivCancel = (ImageView) rowView.findViewById(R.id.ivCancel);

      //  ivImg.setImageBitmap(getBackground(listPathFrames.get(pos)));



        return rowView;
    }

    private Bitmap getBackground(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bitmap = BitmapUtil.resampleImage(filePath, Config.SCREENWIDTH);

        return bitmap;

    }

}
