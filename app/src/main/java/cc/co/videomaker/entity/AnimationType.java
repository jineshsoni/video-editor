package cc.co.videomaker.entity;

/**
 * Created by admin on 12/16/2015.
 */
public enum AnimationType {
    ALPHA, ROTATE, HORIZION_LEFT, HORIZION_RIGHT, HORIZON_CROSS, SCALE, FLIP_HORIZON, FLIP_VERTICAL
}
