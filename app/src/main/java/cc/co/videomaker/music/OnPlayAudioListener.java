package cc.co.videomaker.music;


public interface OnPlayAudioListener {
    public void finish();
    public void duration(long duration);
}
