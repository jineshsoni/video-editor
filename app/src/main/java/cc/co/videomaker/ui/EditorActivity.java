package cc.co.videomaker.ui;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import cc.co.videomaker.BaseActivity;
import cc.co.videomaker.FrameVideoActivity;
import cc.co.videomaker.HomeActivity;
import cc.co.videomaker.SetAudioActivity;

import com.hiep.video.maker.R;
import com.yalantis.ucrop.UCrop;

import cc.co.videomaker.SelectImageActivity;
import cc.co.videomaker.adapter.CustomAdapter;
import cc.co.videomaker.adapter.PhotoGridAdapter;
import cc.co.videomaker.asyncloader.IDoBackGround;
import cc.co.videomaker.system.Config;
import cc.co.videomaker.ui.adapter.MyAdapter;
import cc.co.videomaker.ui.edit.StickerFragment;
import cc.co.videomaker.ui.edit.entity.ItemView;
import cc.co.videomaker.ui.widget.StickerViewEdit;
import cc.co.videomaker.util.BitmapUtil;
import cc.co.videomaker.util.FileUtil;
import cc.co.videomaker.util.LayoutUtil;
import cc.co.videomaker.util.Logger;
import cc.co.videomaker.util.UtiLibs;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by hiep on 6/10/2016.
 */
public class EditorActivity extends BaseActivity {
    public static final int TAB_SIZE = 10;
    public static final int FILTER_RESULT = 1000;
    private static final String TAG = EditorActivity.class.getSimpleName();
    /// Add UI boder
    int[] iconBorderlist;
    private RelativeLayout mContentMainALl, mContentRootView;
    private StickerFragment stickerFragment;
    private TextFragment textFragment;
    private BackgroundFragment backgroundFragment;
    private FilterFragment filterFragment;
    private StickerViewEdit mCurrentView;
    StickerViewEdit stickerViewEdit = null;
    ItemView itemView = null;
    private boolean checkSave = false;
    private String filePathNew = "";
    private String filePathSave = "";
    private Bitmap bitmapMain;
    //////////////UI
    private LinearLayout llSave, llBackMain;
    private TextView mTvCountFrame;
    private ImageView ivBackgroundBlur;
    private View[] viewMenuBottomList;
    private ViewFlipper mViewFlipperEditor;
    private RecyclerView recyclerView_Border;
    private ImageView mBorderImage, mFlterImage;
    private int indexFrame = 0;
    private ArrayList<String> listPathFrames = new ArrayList<>();

    private Bitmap bitmapTmp;

    //GridView gridView;
    //PhotoGridAdapter photoGridAdapter;

    RecyclerView recyclerView;
    CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        init();
    }

    public void init() {
        iniUI();
        addAdmobBaner(R.id.llBottomAds);
        //showAdmobFullScreend();;
        iniEditor();
        getData();

        findViewById(R.id.ivAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addAdmobBaner(R.id.llBottomAds);

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditorActivity.this, FrameVideoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });


    }

    @Override
    public void iniUI() {
        llSave = (LinearLayout) findViewById(R.id.llSave);
        //gridView = (GridView) findViewById(R.id.gridView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        llSave.setOnClickListener(this);
        mTvCountFrame = (TextView) findViewById(R.id.tv_frame_count);
        llBackMain = (LinearLayout) findViewById(R.id.llBackMain);
        llBackMain.setOnClickListener(this);

        //frameLayoutMirror = (FrameLayout) findViewById(R.id.frameLayoutMirror);
        mContentMainALl = (RelativeLayout) findViewById(R.id.rlMainALl);
        mContentMainALl.getLayoutParams().width = Config.SCREENWIDTH;
        mContentMainALl.getLayoutParams().height = Config.SCREENWIDTH;

        mContentRootView = (RelativeLayout) findViewById(R.id.rlMain);

        mViewFlipperEditor = (ViewFlipper) this.findViewById(R.id.view_flipper_editor);
        mViewFlipperEditor.setDisplayedChild(7);

        recyclerView_Border = (RecyclerView) findViewById(R.id.recyclerView_Border);

        ivBackgroundBlur = (ImageView) findViewById(R.id.ivBackgroundBlur);

        findViewById(R.id.iv_crop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filePathSave = FileUtil.getImageInput() + "/" + (indexFrame - 1) + ".jpg";
                UCrop.of(Uri.fromFile(new File(listPathFrames.get(indexFrame - 1))),
                        Uri.fromFile(new File(filePathSave)))
                        .withAspectRatio(1, 1)
                        .withMaxResultSize(Config.SCREENWIDTH, Config.SCREENWIDTH)
                        .start(EditorActivity.this);
            }
        });

    }

    private void iniEditor() {
        initBackgroundUI();
        initFilterUI();
        addBorder();
        initEmojiUI();
        initAddTextUI();
    }

    public void setAdapter() {
        //listPathFrames = (ArrayList<String>) getIntent().getSerializableExtra(SelectImageActivity.EXTAR_PHOTO_PATH_LIST);
        customAdapter = new CustomAdapter(listPathFrames, new OnRemoveImg() {
            @Override
            public void onRemove(final int i) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listPathFrames.remove(i);
                        listPathFrames.trimToSize();
                        if (listPathFrames.size() == 0)
                            finish();
                        mTvCountFrame.setText(indexFrame + "/" + listPathFrames.size());
                        setAdapter();
                    }
                });
            }

            @Override
            public void onClick(int pos) {
                customAdapter.notifyDataSetChanged();
                filePathNew = listPathFrames.get(pos);
                Logger.d(pos + ". filePathNew: " + filePathNew);
                if (filePathNew != null) {
                    addBackground2(filePathNew);
                }
                indexFrame = pos;
                indexFrame++;
                mTvCountFrame.setText(indexFrame + "/" + listPathFrames.size());
            }
        });
        recyclerView.setAdapter(customAdapter);

    }

    public void clearViewFlipper() {
        this.mViewFlipperEditor.setDisplayedChild(7);
        this.setTabMenuBottom(-1);
    }

    private void setTabMenuBottom(int tab) {
        if (this.viewMenuBottomList == null) {
            this.viewMenuBottomList = new View[TAB_SIZE];
            this.viewMenuBottomList[0] = this.findViewById(R.id.iv_mirror);
            this.viewMenuBottomList[1] = this.findViewById(R.id.iv_blur);
            this.viewMenuBottomList[2] = this.findViewById(R.id.iv_filter);
            this.viewMenuBottomList[3] = this.findViewById(R.id.iv_editor);
            this.viewMenuBottomList[4] = this.findViewById(R.id.iv_background);
            this.viewMenuBottomList[5] = this.findViewById(R.id.iv_border);
            this.viewMenuBottomList[6] = this.findViewById(R.id.iv_emoji);
            this.viewMenuBottomList[7] = this.findViewById(R.id.iv_addtext);
            this.viewMenuBottomList[8] = this.findViewById(R.id.iv_flip);
            this.viewMenuBottomList[9] = this.findViewById(R.id.iv_rotate);
        }

        for (int i = 0; i < this.viewMenuBottomList.length; ++i) {
            this.viewMenuBottomList[i].setBackgroundResource(R.color.app_square);
        }

        if (tab >= 0) {
            this.viewMenuBottomList[tab].setBackgroundResource(R.color.collage_purple);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);

            Log.v("cropped", " - " + resultUri);
            filePathSave = FileUtil.getImageInput() + "/" + (indexFrame - 1) + ".jpg";
            //   BitmapUtil.saveBitmapToLocal(filePathSave,
            //          BitmapUtil.resampleImage(getRealPathFromURI(resultUri), Config.SCREENWIDTH));

            listPathFrames.remove(indexFrame - 1);
            listPathFrames.add(indexFrame - 1, filePathSave);
            customAdapter.notifyDataSetChanged();

            ivBackgroundBlur.setImageURI(resultUri);

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }


    private void getData() {
        Intent mIntent = getIntent();
        if (mIntent != null) {
            listPathFrames = (ArrayList<String>) mIntent.getSerializableExtra(SelectImageActivity.EXTAR_PHOTO_PATH_LIST);
            for (int i = 0; i < listPathFrames.size(); i++) {
                Log.v("saving", " - " + listPathFrames.get(i));
                filePathSave = FileUtil.getImageInput() + "/" + (i) + ".jpg";
                BitmapUtil.saveBitmapToLocal(filePathSave, BitmapUtil.resampleImage(listPathFrames.get(i), Config.SCREENWIDTH));
            }
            //gridView.setNumColumns(listPathFrames.size());
            setAdapter();

            if (listPathFrames != null) {
                nextFrame();
            } else {
                finish();
            }
        } else {
            finish();
        }
    }

    private void nextFrame() {

        filePathNew = listPathFrames.get(indexFrame);
        Logger.d(indexFrame + ". filePathNew: " + filePathNew);
        if (filePathNew != null) {
            addBackground(filePathNew);
        }
        indexFrame++;
        mTvCountFrame.setText(indexFrame + "/" + listPathFrames.size());
    }

    private void nextFrame2() {

        filePathNew = listPathFrames.get(indexFrame);
        Logger.d(indexFrame + ". filePathNew: " + filePathNew);
        if (filePathNew != null) {
            addBackground2(filePathNew);
        }
        indexFrame++;
        mTvCountFrame.setText(indexFrame + "/" + listPathFrames.size());
    }


    private void addBackground(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bitmap = BitmapUtil.resampleImage(filePath, Config.SCREENWIDTH);
        if (bitmap != null) {
            myApplication.setBitmapOld(bitmap);

            bitmapMain = bitmap;

            ivBackgroundBlur.setImageBitmap(bitmapMain);

            mFlterImage = LayoutUtil.createImageView(this, 0, 0, Config.SCREENWIDTH, Config.SCREENWIDTH);
            mContentRootView.addView(mFlterImage);
            mBorderImage = LayoutUtil.createImageView(this, 0, 0, Config.SCREENWIDTH, Config.SCREENWIDTH);
            mContentRootView.addView(mBorderImage);


        } else {
            Toast.makeText(EditorActivity.this, "Images error", Toast.LENGTH_SHORT).show();
        }
    }

    private void addBackground2(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;

        Bitmap bitmap = BitmapUtil.resampleImage(filePath, Config.SCREENWIDTH);
        if (bitmap != null) {
            myApplication.setBitmapOld(bitmap);

            bitmapMain = bitmap;

            ivBackgroundBlur.setImageBitmap(bitmapMain);

            mBorderImage.setImageResource(android.R.color.transparent);
            mFlterImage.setImageResource(android.R.color.transparent);

            mViewFlipperEditor.setDisplayedChild(7);

            if (stickerViewEdit != null)
                mContentRootView.removeView(stickerViewEdit);
            if (itemView != null)
                mContentRootView.removeView(itemView.view);

        } else {
            Toast.makeText(EditorActivity.this, "Images error", Toast.LENGTH_SHORT).show();
        }
    }


    private void addBorder() {
        iconBorderlist = new int[]{
                R.drawable.icon_none, R.drawable.border_1, R.drawable.border_2, R.drawable.border_27, R.drawable.border_28,
                R.drawable.border_3, R.drawable.border_9, R.drawable.border_15, R.drawable.border_21, R.drawable.border_29,
                R.drawable.border_4, R.drawable.border_10, R.drawable.border_16, R.drawable.border_22, R.drawable.border_30,
                R.drawable.border_5, R.drawable.border_11, R.drawable.border_17, R.drawable.border_23, R.drawable.border_31,
                R.drawable.border_6, R.drawable.border_12, R.drawable.border_18, R.drawable.border_24, R.drawable.border_32,
                R.drawable.border_7, R.drawable.border_13, R.drawable.border_19, R.drawable.border_25, R.drawable.border_33,
                R.drawable.border_8, R.drawable.border_14, R.drawable.border_20, R.drawable.border_26, R.drawable.border_34
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(linearLayoutManager.HORIZONTAL);
        int colorFlipper = this.getResources().getColor(R.color.trgb_262626);
        int colorBtnFooter = this.getResources().getColor(R.color.collage_purple);
        MyAdapter collageAdapter = new MyAdapter(iconBorderlist, new MyAdapter.CurrentCollageIndexChangedListener() {
            public void onIndexChanged(int index) {
                Logger.d(TAG, "---- filter >> index: " + index);
                if (index > 0) {
                    Glide.with(EditorActivity.this).load(iconBorderlist[index]).asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                }

                                @Override
                                public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                                    mBorderImage.setImageBitmap(arg0);
                                }
                            });
                } else if (index == 0) {
                    mBorderImage.setImageResource(android.R.color.transparent);
                }
            }
        }, colorFlipper, colorBtnFooter, false, true);

        recyclerView_Border.setLayoutManager(linearLayoutManager);
        recyclerView_Border.setAdapter(collageAdapter);
        recyclerView_Border.setItemAnimator(new DefaultItemAnimator());
    }

    // Add UI Background
    public void initBackgroundUI() {
        backgroundFragment = new BackgroundFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frame_background, backgroundFragment, "background");
        fragmentTransaction.hide(backgroundFragment);
        fragmentTransaction.commit();
    }

    public void changeBackgroundState() {
        if (!backgroundFragment.getShowFragment()) {
            backgroundFragment = (BackgroundFragment) getSupportFragmentManager().findFragmentByTag("background");
            backgroundFragment.show();
        } else {
            return;
        }
    }

    // Add UI Filter
    public void initFilterUI() {
        filterFragment = new FilterFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frame_filter, filterFragment, "Filter");
        fragmentTransaction.hide(filterFragment);
        fragmentTransaction.commit();
    }

    public void changeFilterState() {
        if (!filterFragment.getShowFragment()) {
            filterFragment = (FilterFragment) getSupportFragmentManager().findFragmentByTag("Filter");
            filterFragment.show();
        } else {
            return;
        }
    }

    // Add UI Emoji
    public void initEmojiUI() {
        stickerFragment = new StickerFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frame_emoji, stickerFragment, "Emoji");
        fragmentTransaction.hide(stickerFragment);
        fragmentTransaction.commit();
    }

    public void changeStickerState() {
        if (!stickerFragment.getShowFragment()) {
            stickerFragment = (StickerFragment) getSupportFragmentManager().findFragmentByTag("Emoji");
            stickerFragment.show();
        } else {
            return;
        }
    }

    // Add UI Text
    public void initAddTextUI() {
        textFragment = new TextFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add(R.id.frame_text, textFragment, "Text");
        fragmentTransaction.hide(textFragment);
        fragmentTransaction.commit();
    }

    public void changeAddTextState() {
        if (!textFragment.getShowFragment()) {
            textFragment = (TextFragment) getSupportFragmentManager().findFragmentByTag("Text");
            textFragment.show();
        } else {
            return;
        }
    }

    // add Sticjker
    public void addStickerView(int resource) {
        stickerViewEdit = new StickerViewEdit(this);
        try {
            stickerViewEdit.setImageResource(resource);
            stickerViewEdit.setOperationListener(new StickerViewEdit.OperationListener() {
                @Override
                public void onDeleteClick() {
                    mContentRootView.removeView(stickerViewEdit);
                }

                @Override
                public void onEdit(StickerViewEdit stickerViewEdit) {
                    mCurrentView.setInEdit(false);
                    mCurrentView = stickerViewEdit;
                    mCurrentView.setInEdit(true);
                }

                @Override
                public void onTop(StickerViewEdit stickerViewEdit) {

                }
            });

            final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mContentRootView.addView(stickerViewEdit, lp);
            setCurrentEdit(stickerViewEdit);

        } catch (Exception e) {
            Logger.d(TAG, "-- error resize bitmap  in addStickerView function");
        }
    }

    private void setCurrentEdit(StickerViewEdit stickerViewEdit) {
        if (mCurrentView != null) {
            mCurrentView.setInEdit(false);
        }
        mCurrentView = stickerViewEdit;
        stickerViewEdit.setInEdit(true);
    }

    // Add Text
    public void addStickerText(EditText edtMain) {
        if (edtMain.getText().toString().length() != 0) {
            edtMain.setBackgroundColor(this.getResources().getColor(R.color.app_tranparent));
            edtMain.setCursorVisible(false);
            edtMain.setSelectAllOnFocus(false);
            edtMain.setError(null);
            edtMain.setSelected(false);
            edtMain.clearComposingText();

            Bitmap bitmap = Bitmap.createBitmap(edtMain.getWidth(), edtMain.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            edtMain.layout(0, 0, edtMain.getWidth(), edtMain.getHeight());
            edtMain.draw(canvas);

            StickerViewEdit stickerViewEdit = new StickerViewEdit(this, false);

            stickerViewEdit.setBitmap(BitmapUtil.alPhaBitmap(bitmap, 1), UtiLibs.getStatusBarHeight(this));
            final RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            mContentRootView.addView(stickerViewEdit, lp);
            itemView = new ItemView(stickerViewEdit, bitmap);
            setCurrentEdit(stickerViewEdit);

            stickerViewEdit.setOperationListener(new StickerViewEdit.OperationListener() {
                @Override
                public void onDeleteClick() {
                    mContentRootView.removeView(itemView.view);
                }

                @Override
                public void onEdit(StickerViewEdit stickerViewEdit) {
                    mCurrentView.setInEdit(false);
                    mCurrentView = stickerViewEdit;
                    mCurrentView.setInEdit(true);
                }

                @Override
                public void onTop(StickerViewEdit stickerViewEdit) {
                    mContentRootView.removeView(itemView.view);
                    mContentRootView.addView(stickerViewEdit, lp);
                }
            });

        }
    }

    // Add background
    public void updateBackground(int resource) {
        if (resource == -1) {
            ivBackgroundBlur.setImageBitmap(bitmapMain);
        } else {
            Glide.with(EditorActivity.this).load(resource).asBitmap()
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        }

                        @Override
                        public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                            ivBackgroundBlur.setImageBitmap(arg0);
                        }
                    });
            //ivBackgroundBlur.setImageResource(resource);
        }
    }

    // AddFilter
    public void updateFilter(int resource) {
        Glide.with(EditorActivity.this).load(resource).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    }

                    @Override
                    public void onResourceReady(Bitmap arg0, GlideAnimation<? super Bitmap> arg1) {
                        mFlterImage.setImageBitmap(arg0);
                        mFlterImage.setAlpha(0.2f);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        switch (id) {
            //Menu Bottom
            case R.id.iv_filter:
                this.setTabMenuBottom(2);
                mViewFlipperEditor.setDisplayedChild(2);
                changeFilterState();
                break;
            case R.id.iv_editor:
                this.setTabMenuBottom(3);
                mViewFlipperEditor.setDisplayedChild(7);
                break;
            case R.id.iv_border:
                this.setTabMenuBottom(5);
                mViewFlipperEditor.setDisplayedChild(4);
                break;
            case R.id.iv_emoji:
                this.setTabMenuBottom(6);
                mViewFlipperEditor.setDisplayedChild(5);
                changeStickerState();
                break;
            case R.id.iv_addtext:
                this.setTabMenuBottom(7);
                mViewFlipperEditor.setDisplayedChild(6);
                changeAddTextState();
                break;
            case R.id.iv_flip:
                this.setTabMenuBottom(8);
                mViewFlipperEditor.setDisplayedChild(7);
                flipVImage();
                break;
            case R.id.iv_rotate:
                this.setTabMenuBottom(9);
                mViewFlipperEditor.setDisplayedChild(7);
                rotateImage();
                break;
            case R.id.llBackMain:
                onBackPressed();
                break;
            case R.id.llSave:
                saveContent();
                break;
        }
    }


    private void rotateImage() {
        if (bitmapTmp == null) {
            if (bitmapMain == null) {
                return;
            }
            bitmapMain = BitmapUtil.rotateBitmap(bitmapMain, 90);

            ivBackgroundBlur.setImageBitmap(bitmapMain);


        } else {
            bitmapMain = BitmapUtil.rotateBitmap(bitmapMain, 90);
            bitmapTmp = BitmapUtil.rotateBitmap(bitmapTmp, 90);

            ivBackgroundBlur.setImageBitmap(bitmapTmp);

        }


    }

    private void flipVImage() {
        Logger.e(TAG, "---- flipVImage = " + bitmapTmp);
        if (bitmapTmp == null) {
            if (bitmapMain == null) {
                return;
            }
            bitmapMain = BitmapUtil.flipVBitmap(bitmapMain);

            ivBackgroundBlur.setImageBitmap(bitmapMain);

        } else {
            bitmapMain = BitmapUtil.flipVBitmap(bitmapMain);
            bitmapTmp = BitmapUtil.flipVBitmap(bitmapTmp);

            ivBackgroundBlur.setImageBitmap(bitmapTmp);
        }
    }

    private void saveContent() {
        showLoading();
        filePathSave = FileUtil.getImageInput() + "/" + (indexFrame - 1) + ".jpg";
        Logger.d("filePathSave: " + filePathSave);
        if (mCurrentView != null) {
            mCurrentView.setInEdit(false);
        }
        doBackGround(new IDoBackGround() {
            @Override
            public void onDoBackGround(boolean isCancelled) {
                Bitmap bitmap = Bitmap.createBitmap(mContentRootView.getWidth(), mContentMainALl.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                mContentMainALl.draw(canvas);
                filePathSave = BitmapUtil.saveBitmapToLocal(filePathSave, bitmap);
            }

            @Override
            public void onComplelted() {
                checkSave = true;
                updateMedia(filePathSave);
                listPathFrames.set(indexFrame - 1, filePathSave);
                customAdapter.notifyDataSetChanged();
                dismissLoading();
                /*if (indexFrame == listPathFrames.size()) {
                    Intent intent = new Intent(EditorActivity.this, FrameVideoActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    nextFrame2();
                }*/

            }
        });
    }

    private void saveContent(final int indexFrame) {
        showLoading();
        filePathSave = FileUtil.getImageInput() + "/" + (indexFrame) + ".jpg";
        Logger.d("filePathSave: " + filePathSave);
        if (mCurrentView != null) {
            mCurrentView.setInEdit(false);
        }
        doBackGround(new IDoBackGround() {
            @Override
            public void onDoBackGround(boolean isCancelled) {
                //addBackground2(filePathSave);
                Bitmap bitmap = Bitmap.createBitmap(Config.SCREENWIDTH, Config.SCREENWIDTH, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                ivBackgroundBlur.setImageBitmap(bitmap);
                mContentMainALl.draw(canvas);
                filePathSave = BitmapUtil.saveBitmapToLocal(filePathSave, bitmap);
            }

            @Override
            public void onComplelted() {
                checkSave = true;
                //updateMedia(filePathSave);
                //listPathFrames.set(indexFrame - 1, filePathSave);
                //customAdapter.notifyDataSetChanged();
                dismissLoading();
            }
        });
    }


    @Override
    public void onBackPressed() {
        int displayChild = mViewFlipperEditor.getDisplayedChild();
        if (displayChild != 7) {
            clearViewFlipper();
        } else {
            super.onBackPressed();
        }
    }

    public interface OnRemoveImg {
        void onRemove(int pos);

        void onClick(int pos);
    }

}
